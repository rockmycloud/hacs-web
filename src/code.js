let cBox
let q_lock
document.addEventListener('keyup', event => {if (event.code === 'Space') get_question()})

function get_question(){
    if(!q_lock)
        $.get("https://hacs.rockmy.cloud/api/question", function(data){parse_question(data)})
}

function parse_question(quest){
    $('#q_text').html(quest[0].question)
    $('#q_pic').css('visibility', 'hidden')
    q_lock = true
    if(quest[0].category_id > 0) populate_answers(quest)
}

function populate_answers(quest){
    const answers = quest[0].wrong_answers.concat(quest[0].right_answer)
    let fields = [...Array(quest[0].wrong_answers.length+1).keys()]
    fields = fields.map(i => "answer_box" + i)
    answers.forEach(element => {
        cBox = document.getElementById(fields.splice(Math.floor(Math.random() * fields.length),1))
        $(cBox).html(element)
        $(cBox).css({'backgroundColor':'lightblue','visibility':'visible','border':'3px solid black'})
    })
}

function lock_answer(ans){
    if(q_lock){
        $(ans).css('border', '3px solid orange')
        setTimeout(function() {check_answer(ans)}, 700)
    }
}

function check_answer(ans){
    q_lock = false
    $('#q_pic').css('visibility', 'visible')
    if(ans.id == cBox.id) {
        $(ans).css('backgroundColor', 'lightgreen')
        if(parent.closeIFrame) parent.closeIFrame()
    }
    else $(ans).css('backgroundColor', 'lightcoral')
}